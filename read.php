<?php

include_once 'config/database.php';
include_once 'objects/jugadores.php';

$database = new Database();
$db = $database->getConnection();
 
$p = new Nombretabla($db);
 
$stmt = $p->readAll();
$num = $stmt->rowCount();
if($num>0){
 
    echo "<table class='table table-bordered table-hover'>";
     
        echo "<tr>";
            echo "<th>idjugadores</th>";
            echo "<th>Nombre</th>";
            echo "<th>Fecha de Nacimiento</th>";
            echo "<th>Nacionalidad</th>";
            echo "<th>Posicion</th>";
        echo "</tr>";
         
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            extract($row);
             
            echo "<tr>";
                echo "<td>{$idjugadores}</td>";
                echo "<td>".utf8_encode($nombre)."</td>";
                echo "<td>{$fecha_nacimiento}</td>";
                echo "<td>{$nacionalidad}</td>";
                echo "<td>{$posicion}</td>";
            echo "</tr>";
        }
         
    echo "</table>";
     
} else{
    echo "<div class='alert alert-info'>No se encontraron registro.</div>";
}
?>